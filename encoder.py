# BWT + LZW

def binary_search(list_to_search, value):
    start = 0
    end = len(list_to_search) - 1

    while start <= end:
        mid = (start + end) // 2
        if list_to_search[mid] > value:
            end = mid - 1
        elif list_to_search[mid] < value:
            start = mid + 1
        else:
            return mid


def bwt():
    input_string = open("input.txt", "r", encoding="utf-8").read()
    rotation_table = [
        input_string[index:] + input_string[:index] for index in range(len(input_string))
    ]
    sorted_table = sorted(rotation_table)
    last_letter = [row[-1] for row in sorted_table]
    bwt_result = "".join(last_letter)
    input_str_index = binary_search(sorted_table, input_string)
    return bwt_result, input_str_index


def lzw(input_string, bw_index=''):
    dict_size = 256
    lzw_dict = {chr(i + 32): i for i in range(dict_size)}

    worked_str = ""
    lzw_result = []
    for char in input_string:
        working_str = worked_str + char
        if working_str in lzw_dict:
            worked_str = working_str
        else:

            lzw_result.append(lzw_dict[worked_str])

            lzw_dict[working_str] = dict_size
            dict_size += 1
            worked_str = char

    lzw_result.append(lzw_dict[worked_str])
    output = ""
    for symbol in lzw_result:
        output += bin(symbol)[2:].zfill(8)
        with open("output.txt", "w") as output_file:
            output_file.write(f"{output} \n{bw_index + 1}")
    return lzw_result


if __name__ == '__main__':
    bw_result, bw_index = bwt()
    print('Преобразование Барроуза-Уилера:\n', bw_result, bw_index)
    lzw_result = lzw(bw_result, bw_index)
    print('Результат сжатия LZW:\n', lzw_result, bw_index)
